(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [
		{name:"mesientomal_atlas_", frames: [[0,5410,1920,1080],[1922,5410,1920,1080],[1922,3246,1920,1080],[3844,5410,1920,1080],[5766,3246,1920,1080],[1922,4328,1920,1080],[1922,0,1920,1080],[3844,0,1920,1080],[1922,2164,1920,1080],[3844,3246,1920,1080],[1922,6492,1920,1080],[3844,4328,1920,1080],[0,6492,1920,1080],[5766,2164,1920,1080],[3844,2164,1920,1080],[5766,5410,1920,1080],[5766,4328,1920,1080],[5766,0,1920,1080],[0,1082,1920,1080],[1922,1082,1920,1080],[3844,1082,1920,1080],[5766,1082,1920,1080],[0,0,1920,1080],[0,2164,1920,1080],[0,3246,1920,1080],[0,4328,1920,1080]]}
];


// symbols:



(lib.f360_mesientomal00 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal01 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal02 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal03 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal04 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal05 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal06 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal07 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal08 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal09 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal10 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal11 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal12 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal13 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal14 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal15 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal16 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal17 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal18 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal19 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal20 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal21 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal22 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal23 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal24 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.f360_mesientomal25 = function() {
	this.spriteSheet = ss["mesientomal_atlas_"];
	this.gotoAndStop(25);
}).prototype = p = new cjs.Sprite();



(lib.mesientomal_clip = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f360_mesientomal00();
	this.instance.parent = this;
	this.instance.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_1 = new lib.f360_mesientomal01();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_2 = new lib.f360_mesientomal02();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_3 = new lib.f360_mesientomal03();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_4 = new lib.f360_mesientomal04();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_5 = new lib.f360_mesientomal05();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_6 = new lib.f360_mesientomal06();
	this.instance_6.parent = this;
	this.instance_6.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_7 = new lib.f360_mesientomal07();
	this.instance_7.parent = this;
	this.instance_7.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_8 = new lib.f360_mesientomal08();
	this.instance_8.parent = this;
	this.instance_8.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_9 = new lib.f360_mesientomal09();
	this.instance_9.parent = this;
	this.instance_9.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_10 = new lib.f360_mesientomal10();
	this.instance_10.parent = this;
	this.instance_10.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_11 = new lib.f360_mesientomal11();
	this.instance_11.parent = this;
	this.instance_11.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_12 = new lib.f360_mesientomal12();
	this.instance_12.parent = this;
	this.instance_12.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_13 = new lib.f360_mesientomal13();
	this.instance_13.parent = this;
	this.instance_13.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_14 = new lib.f360_mesientomal14();
	this.instance_14.parent = this;
	this.instance_14.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_15 = new lib.f360_mesientomal15();
	this.instance_15.parent = this;
	this.instance_15.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_16 = new lib.f360_mesientomal16();
	this.instance_16.parent = this;
	this.instance_16.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_17 = new lib.f360_mesientomal17();
	this.instance_17.parent = this;
	this.instance_17.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_18 = new lib.f360_mesientomal18();
	this.instance_18.parent = this;
	this.instance_18.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_19 = new lib.f360_mesientomal19();
	this.instance_19.parent = this;
	this.instance_19.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_20 = new lib.f360_mesientomal20();
	this.instance_20.parent = this;
	this.instance_20.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_21 = new lib.f360_mesientomal21();
	this.instance_21.parent = this;
	this.instance_21.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_22 = new lib.f360_mesientomal22();
	this.instance_22.parent = this;
	this.instance_22.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_23 = new lib.f360_mesientomal23();
	this.instance_23.parent = this;
	this.instance_23.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_24 = new lib.f360_mesientomal24();
	this.instance_24.parent = this;
	this.instance_24.setTransform(-247.1,-138.9,0.257,0.257);

	this.instance_25 = new lib.f360_mesientomal25();
	this.instance_25.parent = this;
	this.instance_25.setTransform(-247.1,-138.9,0.257,0.257);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_13}]},1).to({state:[{t:this.instance_14}]},1).to({state:[{t:this.instance_15}]},1).to({state:[{t:this.instance_16}]},1).to({state:[{t:this.instance_17}]},1).to({state:[{t:this.instance_18}]},1).to({state:[{t:this.instance_19}]},1).to({state:[{t:this.instance_20}]},1).to({state:[{t:this.instance_21}]},1).to({state:[{t:this.instance_22}]},1).to({state:[{t:this.instance_23}]},1).to({state:[{t:this.instance_24}]},1).to({state:[{t:this.instance_25}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-247.1,-138.9,494.4,278.1);


// stage content:
(lib.mesientomal = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.mesientomal_clip();
	this.instance.parent = this;
	this.instance.setTransform(640.1,360.1,0.667,0.667,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(1115.3,627.4,329.6,185.4);
// library properties:
lib.properties = {
	width: 1280,
	height: 720,
	fps: 30,
	color: "#999999",
	opacity: 1.00,
	manifest: [
		{src:"images/mesientomal_atlas_.png?1510899064361", id:"mesientomal_atlas_"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;