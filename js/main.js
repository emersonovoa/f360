/*!
 * Script Files
 * @since       1.0.0
 * @author      mullenLowe
 * @license     MIT
 */
document.addEventListener('DOMContentLoaded', function () {

  // Get all "navbar-burger" elements
  var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {

    // Add a click event on each of them
    $navbarBurgers.forEach(function ($el) {
      $el.addEventListener('click', function () {

        // Get the target from the "data-target" attribute
        var target = $el.dataset.target;
        var $target = document.getElementById(target);

        // Toggle the class on both the "navbar-burger" and the "navbar-menu"
        $el.classList.toggle('is-active');
        $target.classList.toggle('is-active');

      });
    });
  }

});

/* Animacion de rotar circulo de logo */
$(document).ready(function () {
      // Declarar variables
          var $rotator = $('#rotator');
          var $rotatorb = $('#rotatorb');
          $body = $(document.body),
          bodyHeight = $body.height(),
          windowHeight = $(window).height();

      $(window).scroll(function () {

         // Calcular las porciones de rotacion
         var top = document.documentElement.scrollTop || document.body.scrollTop,
             rot = (top / (bodyHeight - windowHeight) * 360);

         // Verificar que la rotacion llegue a 360 grados
         if (rot > 360) {
            rot = 360;
         }

         // Rotar el circulo con css transforms y vendor prefixes
         $rotator.css({ 'transform': 'rotate(' + rot + 'deg)' });
         $rotator.css({ '-webkit-transform': 'rotate(' + rot + 'deg)' });
         $rotator.css({ '-moz-transform': 'rotate(' + rot + 'deg)' });
         $rotator.css({ '-ms-transform': 'rotate(' + rot + 'deg)' });
         $rotator.css({ '-o-transform': 'rotate(' + rot + 'deg)' });
         $rotator.html('<div>' + rot.toFixed() + ' &deg;</div>');


         // Calcular las porciones de rotacion
         var topb = document.documentElement.scrollTop || document.body.scrollTop,
             rotb = (topb / (800 - 600) * 360);

         // Verificar que la rotacion llegue a 360 grados
         if (rotb > 360) {
            rotb = 360;
         }

         // Rotar el circulo con css transforms y vendor prefixes
         $rotatorb.css({ 'transform': 'rotate(' + rotb + 'deg)' });
         $rotatorb.css({ '-webkit-transform': 'rotate(' + rotb + 'deg)' });
         $rotatorb.css({ '-moz-transform': 'rotate(' + rotb + 'deg)' });
         $rotatorb.css({ '-ms-transform': 'rotate(' + rotb + 'deg)' });
         $rotatorb.css({ '-o-transform': 'rotate(' + rotb + 'deg)' });
         $rotatorb.html('<div>' + rotb.toFixed() + ' &deg;</div>');

      });
   })
